export function formatString(
  filename: string,
  params: Record<string, string>,
): string {
  for (const property in params) {
    filename = filename.replace('{' + property + '}', params[property]);
  }
  return filename;
}

export function csvToObject<T>(
  headersLine: string,
  line: string,
  separator = ';',
): T {
  const object: Record<string, unknown> = {};
  const headers = headersLine.split(separator);
  const values = line.split(separator);
  for (const index in headers) {
    object[headers[index]] = values[index];
  }
  return object as T;
}

export function arrayToCsv<T>(values: T[]): string {
  const lines = [];
  lines.push(getCsvHeaderFromObject(values[0]));
  for (const value of values) {
    lines.push(objectToCsv(value));
  }
  return lines.join('\n');
}

function getCsvHeaderFromObject<T>(object: T): string {
  let header = '';
  for (const key in object) {
    header += key + ';';
  }
  return header.slice(0, -1);
}

function objectToCsv<T>(object: T): string {
  let line = '';
  for (const key in object) {
    line += (object[key] ?? '') + ';';
  }
  return line.slice(0, -1);
}
