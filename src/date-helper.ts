export function getDateTime(offset = 0): number {
  return Date.now() - offset * 24 * 60 * 60 * 1000;
}

export function getDate(offset = 0): number {
  const date = new Date(getDateTime(offset));
  date.setHours(0);
  date.setMinutes(0);
  date.setSeconds(0);
  date.setMilliseconds(0);
  return date.getTime();
}
