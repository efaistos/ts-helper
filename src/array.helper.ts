export function groupBy<T>(
  list: T[],
  keyGetter: (item: T) => string,
): { key: string; values: T[] }[] {
  const map: { key: string; values: T[] }[] = [];
  list.forEach((item) => {
    const key = keyGetter(item);
    const collection = map.find((x) => x.key === key);
    if (!collection) {
      map.push({ key: key, values: [item] });
    } else {
      collection.values.push(item);
    }
  });
  return map;
}

export function sum<T>(array: T[], keyGetter: (item: T) => number): number {
  return array.map(keyGetter).reduce((p, c) => {
    return p + c;
  });
}

export function toBatches<T>(items: T[], size: number): T[][] {
  const batches: T[][] = [];
  while (items.length > 0) {
    batches.push(items.splice(0, size));
  }
  return batches;
}

export async function process<T, U>(
  data: T[],
  action: (params?: T[]) => Promise<U | U[]>,
  async: boolean,
  batchSize = 1,
): Promise<U[]> {
  if (async) {
    return processAsync(data, action, batchSize);
  }
  return processSync(data, action, batchSize);
}

export async function processSync<T, U>(
  data: T[],
  action: (params?: T[]) => Promise<U | U[]>,
  batchSize = 1,
): Promise<U[]> {
  const batches = toBatches(data, batchSize);
  const results: U[] = [];

  for (const batch of batches) {
    const result = await action(batch);
    if (result instanceof Array) {
      results.push(...result);
    } else {
      results.push(result);
    }
  }

  return results;
}

export async function processAsync<T, U>(
  data: T[],
  action: (params?: T[]) => Promise<U | U[]>,
  batchSize = 1,
): Promise<U[]> {
  const batches = toBatches(data, batchSize);

  const results = await Promise.all(batches.map((batch) => action(batch)));
  if (results.length === 0) {
    return [];
  }

  if (results[0] instanceof Array) {
    const resultArray = results as U[][];
    return resultArray.reduce((accumulator, value) =>
      accumulator.concat(value),
    );
  }

  return results as U[];
}
